package es.upm.dit.isst.redfor.servlets;


import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import es.upm.dit.isst.redfor.model.Actualizacion;
import es.upm.dit.isst.redfor.model.Ticket;

@WebServlet("/ServletCreateActualizacion")
@DeclareRoles({ "aeb", "ac" })
@ServletSecurity(@HttpConstraint(rolesAllowed = { "aeb", "ac" }))
@MultipartConfig
public class ServletCreateActualizacion extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Client c = ClientBuilder.newClient(new ClientConfig());
		int idTicket = Integer.parseUnsignedInt(request.getParameter("idTicket"));
		Ticket ticket = null;
		try {
			ticket = c.target("http://localhost:8080/REDFOR-SERVICE/rest/tickets/" + idTicket).request()
					.accept(MediaType.APPLICATION_JSON).get(Ticket.class);
		} catch (Exception e) {
			throw e;
		}
		if (ticket != null) {
			Actualizacion actualizacion = new Actualizacion();

			actualizacion.setDescripcion(request.getParameter("descripcion"));

			Part filePart = request.getPart("foto");// EXCEPTION
			InputStream fileContent = filePart.getInputStream();// EXCEPTION
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			for (int length = 0; (length = fileContent.read(buffer)) > 0;) {// EXCEPTION
				output.write(buffer, 0, length);
			}
			actualizacion.setFoto(output.toByteArray());

			actualizacion.setEmailAgenteAutor(request.getUserPrincipal().getName());

			actualizacion.setTicket(ticket);

			actualizacion.setTimestamp(System.currentTimeMillis());

			c.target("http://localhost:8080/REDFOR-SERVICE/rest/tickets/" + idTicket + "/actualizaciones").request()
					.post(Entity.entity(actualizacion, MediaType.APPLICATION_JSON), Response.class);
		}
		
		try {
			List<Actualizacion> actualizaciones = c
					.target("http://localhost:8080/REDFOR-SERVICE/rest/tickets/" + idTicket + "/actualizaciones")
					.request().accept(MediaType.APPLICATION_JSON).get(new GenericType<List<Actualizacion>>() {
					});
			Collections.reverse(actualizaciones);

			String dirPath = getServletContext().getRealPath("images");
			new File(dirPath).mkdirs();
			List<String> filePaths = new ArrayList<String>();
			for (Actualizacion ai : actualizaciones) {
				if (ai.getFoto() != null && ai.getFoto().length > 0) {
					ByteArrayInputStream i = new ByteArrayInputStream(ai.getFoto());
					BufferedImage bImage = ImageIO.read(i);

					String filePath = String.valueOf(ai.getTimestamp()) + ".jpg";
					String path = dirPath + File.separator + filePath;
					ImageIO.write(bImage, "jpg", new File(path));
					System.out.println("Imagen creada en la ruta " + path);
					filePaths.add(filePath);
				} else {
					filePaths.add("hola");
				}
			}

			request.setAttribute("idTicket", idTicket);
			request.setAttribute("a", actualizaciones);
			request.setAttribute("filePaths", filePaths);

			if (request.isUserInRole("aeb")) {
				request.setAttribute("rol", "aeb");
			} else {
				request.setAttribute("rol", "ac");
			}
			getServletContext().getRequestDispatcher("/Actualizaciones.jsp").forward(request, response);
		} catch (Exception e) {
			throw e;
		}

	}

}
