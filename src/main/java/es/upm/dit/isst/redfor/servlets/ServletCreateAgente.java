package es.upm.dit.isst.redfor.servlets;

import java.io.IOException;

import javax.annotation.security.DeclareRoles;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import es.upm.dit.isst.redfor.model.Agente;

@WebServlet("/ServletCreateAgente")
/**@DeclareRoles("aeb")
@ServletSecurity(@HttpConstraint(rolesAllowed = "aeb"))*/
public class ServletCreateAgente extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		Agente a = new Agente();

		a.setDisponible(true);

		a.setEmail(req.getParameter("email"));

		try {
			a.setTelefono(Integer.parseUnsignedInt(req.getParameter("telefono")));
		} catch (NumberFormatException e) {
			throw e;
		}

		a.setNombreApellidos(req.getParameter("nombreApellidos"));

		a.setOficio(req.getParameter("oficio"));

		a.setPassword(req.getParameter("password"));

		a.setRol(req.getParameter("rol"));

		Client c = ClientBuilder.newClient(new ClientConfig());
		c.target("http://localhost:8080/REDFOR-SERVICE/rest/agentes").request()
				.post(Entity.entity(a, MediaType.APPLICATION_JSON), Response.class);
		getServletContext().getRequestDispatcher("/Admin.jsp").forward(req, resp);
	}

}
