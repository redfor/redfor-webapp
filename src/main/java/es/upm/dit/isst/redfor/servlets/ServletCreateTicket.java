package es.upm.dit.isst.redfor.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.security.DeclareRoles;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import es.upm.dit.isst.redfor.enums.TipoTicket;
import es.upm.dit.isst.redfor.model.Agente;
import es.upm.dit.isst.redfor.model.Ticket;
import es.upm.dit.isst.redfor.model.Nodo;

@WebServlet("/ServletCreateTicket")
@DeclareRoles("aeb")
@ServletSecurity(@HttpConstraint(rolesAllowed = "aeb"))
public class ServletCreateTicket extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		Client c = ClientBuilder.newClient(new ClientConfig());
		int idNodo = Integer.parseUnsignedInt(req.getParameter("idNodo"));// EXCEPTION
		Nodo nodo = null;
		try {
			nodo = c.target("http://localhost:8080/REDFOR-SERVICE/rest/nodos/" + idNodo).request()
					.accept(MediaType.APPLICATION_JSON).get(Nodo.class);
		} catch (Exception e) {
			throw e;
		}
		if (nodo != null) {
			// parte de crear el ticket nuevo
			//
			Ticket ticket = new Ticket();

			ticket.setActivo(true);

			ticket.setDescripcion(req.getParameter("descripcion"));

			ticket.setEmailAgenteAutor(req.getUserPrincipal().getName());

			ticket.setNodo(nodo);

			ticket.setTimestamp(System.currentTimeMillis());

			String tipo = req.getParameter("tipo");
			if ("Alto".equals(tipo)) {
				ticket.setTipo(TipoTicket.ALTO_RIESGO);
			} else if ("Medio".equals(tipo)) {
				ticket.setTipo(TipoTicket.MEDIO_RIESGO);
			} else if ("Bajo".equals(tipo)) {
				ticket.setTipo(TipoTicket.BAJO_RIESGO);
			} else if ("Mantenimiento".equals(tipo)) {
				ticket.setTipo(TipoTicket.MANTENIMIENTO);
			} else if ("Conato".equals(tipo)) {
				ticket.setTipo(TipoTicket.CONATO_INCENDIO);
			}

			ticket.setTitulo(req.getParameter("titulo"));

			String[] emails = req.getParameterValues("emailsAgentesAsignados");
			List<String> emailsAgentesAsignados = new ArrayList<String>();
			for (String email : emails) {
				Agente agente = null;
				try {
					agente = c.target("http://localhost:8080/REDFOR-SERVICE/rest/agentes/" + email).request()
							.accept(MediaType.APPLICATION_JSON).get(Agente.class);
				} catch (Exception e) {
					throw e;
				}
				if (agente != null) {
					agente.setDisponible(false);
					c.target("http://localhost:8080/REDFOR-SERVICE/rest/agentes/" + email).request()
							.post(Entity.entity(agente, MediaType.APPLICATION_JSON), Response.class);
				}
				emailsAgentesAsignados.add(email);
			}
			ticket.setEmailsAgentesAsignados(emailsAgentesAsignados);

			c.target("http://localhost:8080/REDFOR-SERVICE/rest/tickets/").request()
					.post(Entity.entity(ticket, MediaType.APPLICATION_JSON), Response.class);

			//
			// parte de crear el ticket nuevo

			List<Ticket> tickets = c.target("http://localhost:8080/REDFOR-SERVICE/rest/tickets").request()
					.accept(MediaType.APPLICATION_JSON).get(new GenericType<List<Ticket>>() {
					});
			// ver si hay activos para saber si dibujar la tabla en MapaTickets.jsp o no

			boolean hayActivos = false;
			for (Ticket ti : tickets) {
				if (ti.isActivo() == true) {
					hayActivos = true;
					break;
				}
			}
			req.setAttribute("hayActivos", hayActivos);
			req.setAttribute("t", tickets);
			getServletContext().getRequestDispatcher("/MapaTickets.jsp").forward(req, resp);

		}

	}
}
