package es.upm.dit.isst.redfor.servlets;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.security.DeclareRoles;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.client.ClientConfig;
import es.upm.dit.isst.redfor.model.Actualizacion;

@WebServlet("/ServletReadActualizaciones")
@DeclareRoles({ "aeb", "ac" })
@ServletSecurity(@HttpConstraint(rolesAllowed = { "aeb", "ac" }))
public class ServletReadActualizaciones extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			int idTicket = Integer.parseUnsignedInt(request.getParameter("idTicket"));
			Client c = ClientBuilder.newClient(new ClientConfig());
			List<Actualizacion> actualizaciones = c
					.target("http://localhost:8080/REDFOR-SERVICE/rest/tickets/" + idTicket + "/actualizaciones")
					.request().accept(MediaType.APPLICATION_JSON).get(new GenericType<List<Actualizacion>>() {
					});
			Collections.reverse(actualizaciones);

			String dirPath = getServletContext().getRealPath("images");
			new File(dirPath).mkdirs();
			List<String> filePaths = new ArrayList<String>();
			for (Actualizacion ai : actualizaciones) {
				if (ai.getFoto() != null && ai.getFoto().length > 0) {
					ByteArrayInputStream i = new ByteArrayInputStream(ai.getFoto());
					BufferedImage bImage = ImageIO.read(i);

					String filePath = String.valueOf(ai.getTimestamp()) + ".jpg";
					String path = dirPath + File.separator + filePath;
					ImageIO.write(bImage, "jpg", new File(path));
					System.out.println("Imagen creada en la ruta " + path);
					filePaths.add(filePath);
				} else {
					filePaths.add("hola");
				}
			}

			request.setAttribute("idTicket", idTicket);
			request.setAttribute("a", actualizaciones);
			request.setAttribute("filePaths", filePaths);
			
			response.setIntHeader("Refresh", 15);

			if (request.isUserInRole("aeb")) {
				request.setAttribute("rol", "aeb");
			} else {
				request.setAttribute("rol", "ac");
			}
			getServletContext().getRequestDispatcher("/Actualizaciones.jsp").forward(request, response);
		} catch (Exception e) {
			throw e;
		}
	}

}