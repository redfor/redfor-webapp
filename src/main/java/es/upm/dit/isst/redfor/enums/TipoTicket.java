package es.upm.dit.isst.redfor.enums;

public enum TipoTicket {
	BAJO_RIESGO, MEDIO_RIESGO, ALTO_RIESGO, MANTENIMIENTO, CONATO_INCENDIO
}
