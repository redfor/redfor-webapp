package es.upm.dit.isst.redfor.servlets;

import java.io.IOException;
import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import es.upm.dit.isst.redfor.model.Nodo;
import es.upm.dit.isst.redfor.model.Ticket;

@WebServlet("/ServletCreateNodo")
@DeclareRoles("aeb")
@ServletSecurity(@HttpConstraint(rolesAllowed = "aeb"))
public class ServletCreateNodo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Nodo ni = new Nodo();
		ni.setLatitud(Double.parseDouble(req.getParameter("latitud").replace(",", ".")));
		ni.setLongitud(Double.parseDouble(req.getParameter("longitud").replace(",", ".")));
		Client c = ClientBuilder.newClient(new ClientConfig());
		c.target("http://localhost:8080/REDFOR-SERVICE/rest/nodos").request()
				.post(Entity.entity(ni, MediaType.APPLICATION_JSON), Response.class);
		List<Nodo> n = c.target("http://localhost:8080/REDFOR-SERVICE/rest/nodos").request()
				.accept(MediaType.APPLICATION_JSON).get(new GenericType<List<Nodo>>() {
				});
		req.setAttribute("n", n);

		// ver si hay activos para saber si dibujar la tabla en MapaTickets.jsp o no
		List<Ticket> t = c.target("http://localhost:8080/REDFOR-SERVICE/rest/tickets").request()
				.accept(MediaType.APPLICATION_JSON).get(new GenericType<List<Ticket>>() {
				});
		req.setAttribute("t", t);
		boolean hayActivos = false;
		for (Ticket ti : t) {
			if (ti.isActivo() == true) {
				hayActivos = true;
				break;
			}
		}
		req.setAttribute("hayActivos", hayActivos);
		getServletContext().getRequestDispatcher("/MapaTickets.jsp").forward(req, resp);

	}

}
