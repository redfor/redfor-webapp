<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
	<head>
		<meta charset="UTF-8">
		<title>AgenteCampo.jsp</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"/>
		<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
		<link rel="stylesheet" href="css/AgenteCampo.css"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
	
	
	
		<span id="test"></span>
		
		
		
		<div id="ticket_y_mapa">
		
			
		
			<div id="t">
			
				<c:if test="${not empty ticket}">
					<span>${ticket.titulo}</span>
		
					<jsp:useBean id="dateObject" class="java.util.Date"/>
					<jsp:setProperty name="dateObject" property="time" value="${ticket.timestamp}"/>
					<fmt:formatDate value="${dateObject}" pattern="hh:mm dd-MMM-yy"/>
					<br><br>
		
					<form action="ServletReadActualizaciones">
						<input type="hidden" name="idTicket" value="${ticket.id}"/>
						<input type="submit" value="Ver Detalles"/>
					</form>
					<br>
				</c:if>

				
				<c:if test="${empty ticket}">
					<p>Por el momento no tiene ticket asociado.</p>
				</c:if>
				
				<%@ include file="LogOut.jsp"%>
				<br>
				
			</div>
			
			
			
			<div id="mapa"></div>
			
			
			
			<script>
			$(document).ready(function(){

				provincia="Madrid";
				
				$.ajax({
					url:"https://public.opendatasoft.com/api/records/1.0/search/?dataset=provincias-espanolas&rows=52",
					type:"get",
					dataType:"JSON",
					cache:false,
					success:function(provincias){

						var records=provincias.records;
						var record=null;
						for(i=0;i<records.length;i++){
							if(records[i].fields.texto==provincia){
								record=records[i];
							}
						}
						var centro=record.geometry.coordinates;
						var map=L.map('mapa').setView([centro[1],centro[0]],8);
						
						L.tileLayer('https://api.maptiler.com/maps/topographique/{z}/{x}/{y}.png?key=76ngdVexoAYKztyaW1fN',{
							tileSize:512,
							zoomOffset:-1,
							minZoom:1,
							attribution:"\u003ca href=\"https://www.maptiler.com/copyright/\" target=\"_blank\"\u003e\u0026copy; MapTiler\u003c/a\u003e \u003ca href=\"https://www.openstreetmap.org/copyright\" target=\"_blank\"\u003e\u0026copy; OpenStreetMap contributors\u003c/a\u003e",
							crossOrigin:true
						}).addTo(map);
						
						var geojsonFeature=record.fields.geo_shape;
						L.geoJSON(geojsonFeature,{
							color:"#808080",
							weight:4,
							fill:false
						}).addTo(map);
						
						var strLatitud="${ticket.nodo.latitud}";
						var strLongitud="${ticket.nodo.longitud}";
						var latitud=parseFloat(strLatitud);
						var longitud=parseFloat(strLongitud);
						
						
						L.marker(
							[latitud,longitud],
								{title:"${ticket.nodo.id}"}
						).addTo(map).on("click",function(n){
							/*map.panTo([latitud,longitud]);
							map.setZoom(13);*/
						});
						map.panTo([latitud,longitud]);
						
						

						/*
						//definición iconos
						var icon_riesgo_bajo=L.icon({
							iconUrl:"https://img.icons8.com/emoji/50/000000/yellow-circle-emoji.png",
							iconSize:[30,30]
						});
						var icon_riesgo_medio=L.icon({
							iconUrl:"https://img.icons8.com/emoji/50/000000/red-circle-emoji.png",
							iconSize:[40,40]
						});
						var icon_riesgo_alto=L.icon({
							iconUrl:"https://img.icons8.com/emoji/50/000000/warning-emoji.png",
							iconSize:[50,50]
						});
						var icon_peace=L.icon({
							iconUrl:"https://img.icons8.com/emoji/50/000000/green-circle-emoji.png",
							iconSize:[20,20]
						});
						*/
					}
				});
			});
			</script>
			
		</div>
	</body>
</html>