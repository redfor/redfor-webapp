<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>CrearAgente.jsp</title>
	</head>
	<body>
		<h2>Crear Agente</h2>
		<form action="ServletCreateAgente">
			<input type="email" name="email" placeholder="Email">
			<br><br>
			<input type="text" name="nombreApellidos" placeholder="Nombre y Apellidos">
			<br><br>
			<input type="text" name="oficio" placeholder="Oficio">
			<br><br>
			<input type="password"  name="password" placeholder="Password">
			<br><br>
			<select name="rol">
				<option value="aeb">Agente de Estación Base</option>
				<option value="ac">Agente de Campo</option>
			</select>
			<br><br>
			<input type="number" name="telefono" min="600000000" max="999999999" placeholder="Telefono">
			<br><br>
			<input type="submit" value="Crear Agente"/>
			<br><br>
		</form>
		
		<button onclick="document.location.href='Admin.jsp'">Volver Atrás</button>
		<br><br>
		<%@ include file="LogOut.jsp"%>
	</body>
</html>